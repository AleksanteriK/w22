import logo from './logo.svg';
import './App.css';
import ComponentName from './ComponentName';

function App() {
  return (
    <div>
    <ComponentName country = "Finland"/>
    <ComponentName country = "Sweden"/>
    </div>
  );
}

export default App;
