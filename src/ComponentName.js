const ComponentName = ({ country }) => {
   return (
      <div>
         <p>Hello {country}!</p>
      </div>
   );
}

export default ComponentName;
